package com.example.customviewdrawingapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.customviewdrawingapp.ui.CanvasView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val changeMode: FloatingActionButton = findViewById(R.id.change_mode)
        val canvasView: CanvasView = findViewById(R.id.canvas_view)
        val clearAll : Button = findViewById(R.id.clear_all)

        changeMode.setOnClickListener {
            when (canvasView.mode) {
                CanvasView.Mode.DRAW -> {
                    changeMode.setImageResource(R.drawable.ic_move)
                    canvasView.mode = CanvasView.Mode.MOVE
                }
                CanvasView.Mode.MOVE -> {
                    changeMode.setImageResource(R.drawable.ic_draw)
                    canvasView.mode = CanvasView.Mode.DRAW

                }
            }
        }
        clearAll.setOnClickListener{
            canvasView.clearAll()
        }

    }
}