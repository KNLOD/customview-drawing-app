package com.example.customviewdrawingapp.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.widget.ImageView
import kotlin.random.Random


class CanvasView @JvmOverloads constructor(
    context : Context,
    attrs : AttributeSet? = null,
    defStyleAttr: Int = 0,

    ) : ImageView(context, attrs, defStyleAttr) {

    private var drawingPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var color = Color.RED
    private var startX : Float = 0.0F
    private var startY : Float = 0.0F
    private var endX : Float = 0.0F
    private var endY : Float = 0.0F
    var mode : Mode = Mode.DRAW
    private var figureBitmap : Bitmap? = null
    private var resultBitmap : Bitmap? = null

    enum class Mode{
        DRAW, MOVE
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (mode == Mode.MOVE){
            figureBitmap?.let {
                //canvas!!.drawBitmap(it, endX - this.width/3, endY-this.height/3, drawingPaint)
                Log.e("MOVE", "Should be drawed y:$endY, x:$endX")
            }
        }

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        Log.e("TOUCH", "Action: ${event?.action}")
        when (event?.action){
            // Beginning of the TouchEvent
            MotionEvent.ACTION_DOWN -> {
                //Log.e("TOUCH", "Action Down")
                startX = event.x
                startY = event.y

                return true

            }
            // Moving
            MotionEvent.ACTION_MOVE -> {
                //Log.e("TOUCH", "Action Move")
                // set end coordinates to draw the line
                endX = event.x
                endY = event.y
                // according to the mode
                // either move or draw figure(line/s)
                when (mode){
                    Mode.DRAW -> {
                        drawFigure()
                    }
                    Mode.MOVE -> {
                        figureBitmap?.let {
                            moveFigure()
                            invalidate()
                        }
                    }
                }
                // After drawing or moving set start Coordingates
                startX = event.x
                startY = event.y
            }

            // Touch ended
            MotionEvent.ACTION_UP -> {
                // Set End coordinates
                endX = event.x
                endY = event.y
                // According to the Mode
                // either clear FigureBitmap
                when (mode){
                    Mode.MOVE -> {
                        figureBitmap?.let {
                            // clear figureBitmap
                            // in order not to duplicate drawings
                            clearFigureBm()
                            // and save moved bitmap to figureBitmap
                            Canvas(figureBitmap!!).drawBitmap(resultBitmap!!, 0F, 0F, drawingPaint)
                        }
                    }
                    Mode.DRAW -> {
                        // change color of Paint
                        drawingPaint.color = getRandomColor()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    fun getRandomColor() : Int {
        val rnd = Random
        return Color.argb(255,
            rnd.nextInt(256),
            rnd.nextInt(256),
            rnd.nextInt(256)
        )
    }

    /**
     * Moves figureBitmap and
     * draws it on resultBitmap
     */
    private fun moveFigure() {
        Log.e("MOVE", "Has moved")
        figureBitmap.let {
            // clear resultBitmap not to duplicate drawings
            clearResultBm()
            // draw changed(moved) figureBitmap on resultBitmap
            Canvas(resultBitmap!!).drawBitmap(figureBitmap!!, endX - width/3, endY - height/3, drawingPaint)
        }
    }

    /**
     * Clears resultBitmap that is showed for the user
     */
    fun clearResultBm(){
        Canvas(resultBitmap!!).drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
    }

    /**
     * Clears figureBitmap that temporal changes(like drawing,
     * moving etc.)
     */
    fun clearFigureBm(){
        Canvas(figureBitmap!!).drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
    }
    /**
     * Clears all bitMaps
     */
    fun clearAll(){
        clearFigureBm()
        clearResultBm()
    }

    /**
     * Sets up BitMaps
     */
    private fun setup(){

        figureBitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
        with(drawingPaint) {
            color = getRandomColor()
            style = Paint.Style.STROKE
            strokeWidth = 10F
        }
        resultBitmap= Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
        with(drawingPaint) {
            color = getRandomColor()
            style = Paint.Style.STROKE
            strokeWidth = 10F
        }
    }

    /**
     * Draws line on figureBitmap and then
     * draws figureBitmap on resultBitmap
     */
    private fun drawFigure(){
        if (figureBitmap == null) {
            setup()
        }

        val figureCanvas: Canvas = Canvas(figureBitmap!!)
        val resultCanvas: Canvas = Canvas(resultBitmap!!)

        // Draw line on figureBitmap
        figureCanvas.drawLine(startX, startY, endX, endY, drawingPaint)
        // Draw figureBitmap on resultBitmap
        resultCanvas.drawBitmap(figureBitmap!!, 0F, 0F, drawingPaint)

        setImageBitmap(resultBitmap!!)

        Log.e("TOUCH", "trying to draw")

    }



}